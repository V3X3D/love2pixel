# love2pixel

A simple pixel art program made with love2d.

### Controls

- Paint = Left Mouse
- Erase = Right Mouse
- Zoom In/Out = Middle Mouse (scroll)
- Move Camera = Middle Mouse (click and drag)
- Grow Canvas = Right Arrow, Down Arrow
- Shrink Canvas = Left Arrow, Up Arrow
- Toggle Grid = "g" Key
- Brush Grow = "=" Key
- Brush Shrink = "-" Key
- Save Image = "s" Key
- Get Color From Canvas = "lctrl" Key + Left Mouse

### Description

love2pixel is a pixel art tool developed to show off the capabilities of my [love2d libraries](https://gitlab.com/V3X3D/love-libs).

### Notes

This program is currently in early development, to see what is being worked on see the TODO.md file.

The saved png file currently goes to love2d's default paths, [shown here](https://love2d.org/wiki/love.filesystem).

![](screenshots/Norm.png)
![](screenshots/Grid.png)

### Links

If you enjoyed this check me out on the following

- [Twitter](https://twitter.com/_V3X3D)
- [Patreon](https://www.patreon.com/V3X3D)
- [Itch](https://v3x3d.itch.io/)

### Credit

[Stacked Pixel](https://monkopus.itch.io/stacked-pixel) -- [cc4](https://creativecommons.org/licenses/by-nd/4.0/) font by monokopus (unchanged).
