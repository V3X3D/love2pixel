SM = require "lib.StackingSceneMgr"
IM = require "lib.InputMgr"
CM = require "lib.CameraMgr"
CL = require "lib.CollisionLib"

-- Brush color value:
-- Might want to find a way to localize this in the future.
-- The "ui" scene modifies it, and the "paint" scene uses it.

color = { 0,0,0,1 }

function love.load()
  SM.setPath("scenes/")
  SM.add("paint")
  SM.add("ui")
end

function love.update(dt)
  SM.update(dt)
  love.keyboard.resetInputStates()
end

function love.draw()
  SM.draw()
end
