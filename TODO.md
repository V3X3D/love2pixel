# love2pixel

The "todo" list for love2pixel, this will be changing frequently.

### Features

[/] Full color selector (by wheel & or value)
[x] Color picker tool (from canvas pixels)
[ ] Brush sizing ui
[ ] Grid toggle ui
[ ] Key swallowing
[ ] Preview view
[ ] Window scaling (full screen, re-sizing)
[ ] Save by path and file name
[ ] Canvas re-size by value
[ ] Key mapping
[ ] User interface

### Bugs

[ ] ui adjustment on screen sizing
[ ] stop paint on canvas when selecting color
[ ] Alpha pixel overlap, rather than replace

### Other

[ ] Documentation work
[x] Add license

### Marks

Finished: [x]
Temporary Solution: [/]
To be worked on: [ ]
