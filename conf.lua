function love.conf(t)
  t.identity = nil                       -- The name of the save directory (string)
  t.version = "11.3"                     -- The LÖVE version this game was made for (string)
  t.console = false                      -- Attach a console (boolean, Windows only)

  -- Window Props
  t.window.title = "love2paint"          -- The window title (string)
  t.window.icon = "imgs/icon.png"        -- Filepath to an image to use as the window's icon (string)
  t.window.width = 960                   -- The window width (number)
  t.window.height = 540                  -- The window height (number)
  t.window.borderless = false            -- Remove all border visuals from the window (boolean)
  t.window.resizable = false             -- Let the window be user-resizable (boolean)
  t.window.minwidth = 960                -- Minimum window width if the window is resizable (number)
  t.window.minheight = 540               -- Minimum window height if the window is resizable (number)
  t.window.fullscreen = false            -- Enable fullscreen (boolean)
  t.window.fullscreentype = "desktop"    -- Choose between "desktop" fullscreen or "normal" fullscreen mode (string)
  t.window.usedpiscale = true            -- Enable automatic DPI scaling (boolean)
  t.window.vsync = false                 -- Vertical sync mode (number)
  t.window.display = 1                   -- Index of the monitor to show the window in (number)
  t.window.highdpi = false               -- Enable high-dpi mode for the window on a Retina display (boolean)

  -- Toggle Modules
  t.modules.audio = false                -- Enable the audio module (boolean)
  t.modules.event = true                 -- Enable the event module (boolean)
  t.modules.font = true                  -- Enable the font module (boolean)
  t.modules.graphics = true              -- Enable the graphics module (boolean)
  t.modules.image = true                 -- Enable the image module (boolean)
  t.modules.joystick = false             -- Enable the joystick module (boolean)
  t.modules.keyboard = true              -- Enable the keyboard module (boolean)
  t.modules.math = true                  -- Enable the math module (boolean)
  t.modules.mouse = true                 -- Enable the mouse module (boolean)
  t.modules.physics = false              -- Enable the physics module (boolean)
  t.modules.sound = false                -- Enable the sound module (boolean)
  t.modules.system = true                -- Enable the system module (boolean)
  t.modules.thread = true                -- Enable the thread module (boolean)
  t.modules.timer = true                 -- Enable the timer module (boolean), Disabling it will result 0 delta time in love.update
  t.modules.window = true                -- Enable the window module (boolean)
end
