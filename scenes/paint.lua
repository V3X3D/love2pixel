local scene = {}
local CM = CM.newManager()

local tile_size = 32
local brush_in_canvas = false

local grid_display = false

local mouse = {
  x = 0,
  y = 0
}

local brush = {
  x = 0,
  y = 0,
  size = 1,
  offset = 0
}

local canvas = {
  map = {},
  x = 0,
  y = 0,
  w = 30,
  h = 17
}

-- In pixels (scales by tile_size)

function scene.load()
  CM.setScale(1) -- Should be set before others (some calls depend on the scale)
  CM.setCoords(love.graphics.getWidth()/2, love.graphics.getHeight()/2)

  -- Generate our canvas
  for y = 1, canvas.h, 1 do
    canvas.map[y] = {}
    for x = 1, canvas.w, 1 do
      -- Fill with alpha 0 pixels
      canvas.map[y][x] = {0, 0, 0, 0}
    end
  end
end

function scene.update(dt)
  -- Setup brush offset
  brush.offset = math.floor(brush.size/2)
  -- Mouse position, duh
  mouse.x, mouse.y = love.mouse.getPosition()
  -- Convert mouse cords (keeps mouse position correct when changing scale)
  mouse.x, mouse.y = CM.toWorldCoords(mouse.x, mouse.y)
  -- Map array mouse position
  brush.x = math.ceil(mouse.x/tile_size)
  brush.y = math.ceil(mouse.y/tile_size)

  -- Track if brush is in the canvas or not
  if CL.pointRect(brush, canvas) then
    brush_in_canvas = true
  else
    brush_in_canvas = false
  end

  --------------
  -- Controls --
  --------------
  -- Color picker
  if love.keyboard.isDown("lctrl") then
    if love.mouse.isDown(1) and brush_in_canvas then
      color = canvas.map[brush.y][brush.x]
    end
  else
    -- Paint
    if love.mouse.isDown(1) and brush_in_canvas then
      for y = -brush.offset, brush.size-brush.offset-1, 1 do
        for x = -brush.offset, brush.size-brush.offset-1, 1 do
          if canvas.map[brush.y+y] ~= nil and canvas.map[brush.y+y][brush.x+x] ~= nil then
            canvas.map[brush.y+y][brush.x+x] = {color[1], color[2], color[3], color[4]}
          end
        end
      end
    end
  end

  -- Erase
  if love.mouse.isDown(2) and brush_in_canvas then
    for y = -brush.offset, brush.size-brush.offset-1, 1 do
      for x = -brush.offset, brush.size-brush.offset-1, 1 do
        if canvas.map[brush.y+y] ~= nil and canvas.map[brush.y+y][brush.x+x] ~= nil then
          canvas.map[brush.y+y][brush.x+x] = {0, 0, 0, 0}
        end
      end
    end
  end

  -- Zoom in/out (scroll)
  function love.wheelmoved(x, y)
    if y > 0 then
      CM.setScale(CM.getScale()+0.08)
    elseif y < 0 then
      CM.setScale(CM.getScale()-0.08)
    end

    if CM.getScale() < 0 then
      CM.setScale(0)
    end
    if CM.getScale() > 3 then
      CM.setScale(3)
    end
  end

  -- Move Camera (pan)
  if love.mouse.isDown(3) then
    -- camera
    local cx, cy = CM.getCoords()
    -- mouse
    function love.mousemoved(mx, my, dx, dy)
      cx = cx+dx
      cy = cy+dy
      CM.setTarget(cx, cy)
    end
  else
    function love.mousemoved(mx, my, dx, dy)
    end
  end

  -- Size The Canvas
  if love.keyboard.isPressed("left") then
    if canvas.w <= 1 then
      canvas.w = 1 -- don't go negative or 0
      return
    end
    for y in ipairs(canvas.map) do
      canvas.map[y][#canvas.map[y]] = nil
    end
    canvas.w = canvas.w-1
  end
  if love.keyboard.isPressed("right") then
    for y in ipairs(canvas.map) do
      canvas.map[y][#canvas.map[y]+1] = {0, 0, 0, 0}
    end
    canvas.w = canvas.w+1
  end
  if love.keyboard.isPressed("up") then
    -- Keep canvas >= 2x2
    if canvas.h <= 1 then
      canvas.h = 1 -- don't go negative or 0
      return
    end
    canvas.map[#canvas.map] = nil
    canvas.h = canvas.h-1
  end
  if love.keyboard.isPressed("down") then
    local t = {}

    for i=1, canvas.w, 1 do
      t[i] = {0, 0, 0, 0}
    end
    canvas.map[#canvas.map+1] = t
    canvas.h = canvas.h+1
  end

  -- Toggle the grid
  if love.keyboard.isPressed("g") then
    grid_display = not grid_display
  end

  -- Change brush size
  if love.keyboard.isPressed("=") then
    brush.size = brush.size+1
  end
  if love.keyboard.isPressed("-") then
    if brush.size-1 ~= 0 then
      brush.size = brush.size-1
    end
  end

  -- Save the png
  if love.keyboard.isPressed("s") then
    print("Saving: love2pixel.png")

    -- Create image of correct size
    local imageData = love.image.newImageData(canvas.w+1, canvas.h+1)
    -- fill image pixels
    for y in ipairs(canvas.map) do
      for x in ipairs(canvas.map[y]) do
        imageData:setPixel(x, y, canvas.map[y][x][1], canvas.map[y][x][2], canvas.map[y][x][3], canvas.map[y][x][4])
      end
    end
    -- encode, and save
    filedata = imageData:encode("png", "love2pixel.png")

    print("Saved: love2pixel.png")
  end

  CM.update(dt)
end

function scene.draw()
  CM.attach() -- Start Camera

  love.graphics.setBackgroundColor(1,1,1,1)

  -- Canvas outline
  love.graphics.setLineWidth(4)
  love.graphics.setColor(0,0,0,1)
  love.graphics.rectangle("line", canvas.x-2, canvas.y-2, canvas.w*tile_size+4, canvas.h*tile_size+4)


  local tmp_x = 0
  local tmp_y = 0

  for y in ipairs(canvas.map) do
    for x in ipairs(canvas.map[y]) do
      -- Subtract tile_size for an offset since base1
      tmp_x = ( x * tile_size ) - tile_size
      tmp_y = ( y * tile_size ) - tile_size

      if canvas.map[y][x] ~= 0 then
        love.graphics.setColor(
          unpack(canvas.map[y][x])
        )
        love.graphics.rectangle("fill", tmp_x, tmp_y, tile_size, tile_size)
      end
    end
  end

  -- Brush color / Visual Brush
  love.graphics.setColor(unpack(color))
  love.graphics.rectangle("fill", (brush.x-brush.offset-1)*tile_size, (brush.y-brush.offset-1)*tile_size, brush.size*tile_size, brush.size*tile_size)

  love.graphics.setLineWidth(1)
  if grid_display then
    love.graphics.setColor(0.5,0.5,1,0.7)

    for y in ipairs(canvas.map) do
      tmp_y = ( y * tile_size ) - tile_size
      love.graphics.line(0, tmp_y, canvas.w*tile_size, tmp_y)
      for x in ipairs(canvas.map[y]) do
        tmp_x = ( x * tile_size ) - tile_size
        love.graphics.line(tmp_x, 0, tmp_x, canvas.h*tile_size)
      end
    end
  end

  CM.detach() -- End Camera
end


return scene
