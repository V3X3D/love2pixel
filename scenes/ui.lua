local scene = {}

local mouse_x, mouse_y = 0, 0
local rgb_font = love.graphics.newFont("fonts/Stacked-Pixel.ttf", 26)

local color_buttons = {
  red = {
    text = "R",
    value = 1,
    x = 14,
    y = 14,
    w = 24,
    h = 24,
    color = {1,0,0,1}
  },
  green = {
    text = "G",
    value = 2,
    x = 14,
    y = 40,
    w = 24,
    h = 24,
    color = {0,1,0,1}
  },
  blue = {
    text = "B",
    value = 3,
    x = 14,
    y = 66,
    w = 24,
    h = 24,
    color = {0,0,1,1}
  },
  alpha = {
    text = "A",
    value = 4,
    x = 14,
    y = 92,
    w = 24,
    h = 24,
    color = {0,0,0,1}
  }
}

function scene.load()
end

function scene.update(dt)
  -- Mouse position, duh
  -- @temporary: need to just swap this for a global mouse
  mouse_x, mouse_y = love.mouse.getPosition()

  --------------
  -- Controls --
  --------------
  -- Select color
  if love.mouse.isPressed(1) then

    -- Increase color
    for k,v in pairs(color_buttons) do
      if CL.pointRect({x=mouse_x, y=mouse_y}, color_buttons[k]) then
        local value = color_buttons[k].value
        color[value] = color[value] + 0.1
      end
    end

    -- Keep colors in bounds
    for i=1,4,1 do
      if color[i] > 1 then
        color[i] = 1
      end
    end

  end

  if love.mouse.isPressed(2) then

    -- Decrease color
    for k,v in pairs(color_buttons) do
      if CL.pointRect({x=mouse_x, y=mouse_y}, color_buttons[k]) then
        local value = color_buttons[k].value
        color[value] = color[value] - 0.1
      end
    end

    -- Keep colors in bounds
    for i=1,4,1 do
      if color[i] < 0 then
        color[i] = 0
      end
    end
  end

end

function scene.draw()
  love.graphics.setFont(rgb_font)
  -- Brush color adjustment color_buttonss
  for k,v in pairs(color_buttons) do
    love.graphics.setColor(unpack(color_buttons[k].color))
    love.graphics.rectangle("fill", color_buttons[k].x, color_buttons[k].y, color_buttons[k].w, color_buttons[k].h)

    -- color value
    love.graphics.setColor(0,0,0)
    love.graphics.print(tostring(color[color_buttons[k].value]), color_buttons[k].x+32, color_buttons[k].y)

    -- Text Shadow
    love.graphics.setColor(0.15,0.15,0.15)
    love.graphics.print(color_buttons[k].text, color_buttons[k].x+4, color_buttons[k].y+1)

    -- Text
    love.graphics.setColor(0.95,0.95,0.95)
    love.graphics.print(color_buttons[k].text, color_buttons[k].x+6, color_buttons[k].y+2)
  end
end

return scene
